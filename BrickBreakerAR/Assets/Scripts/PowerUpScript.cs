using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpScript : MonoBehaviour
{
    public float speed;
    public GameManager gm;
    public Transform paddle;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Da powerup pada dol
        transform.Translate(new Vector2 (0f, -1f) * Time.deltaTime * speed);

        // Da unici object ce ga ne ujames (pac gre izven zaslona)
        if(transform.position.y < -30f) {
            Destroy (gameObject);
        }
    }   
}