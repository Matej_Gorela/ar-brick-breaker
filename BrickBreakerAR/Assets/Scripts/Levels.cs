using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Levels : MonoBehaviour
{
    public enum Difficulty { Easy, Medium, Hard }//Oh look at you Being all fancy-like with your enums XD
    public static Difficulty currentDifficulty;
    public GameObject settingsPanel;
    public Button EasyButton;
    public Button MediumButton;
    public Button HardButton;

    private void Start()
    {
        colorButtons();
    }

    public void Easy()
    {
        //Debug.Log("Level easy");
        currentDifficulty = Difficulty.Easy;
        colorButtons();
    }

    public void Medium()
    {
        //Debug.Log("Level medium");
        currentDifficulty = Difficulty.Medium;
        colorButtons();
    }

    public void Hard()
    {
        //Debug.Log("Level hard");
        currentDifficulty = Difficulty.Hard;
        colorButtons();
    }

    public void Close()
    {
        //SceneManager.LoadScene("Scenes/Menu");
        settingsPanel.SetActive(false);
    }

    //prefarbaj gumbe, da ves katera teznost je izbrana
    public void colorButtons()
    {
        switch (currentDifficulty)
        {
            case Difficulty.Easy:
                EasyButton.image.color = Color.green;
                MediumButton.image.color = Color.gray;
                HardButton.image.color = Color.gray;
                break;
            case Difficulty.Medium:
                EasyButton.image.color = Color.gray;
                MediumButton.image.color = Color.green;
                HardButton.image.color = Color.gray;
                break;
            case Difficulty.Hard:
                EasyButton.image.color = Color.gray;
                MediumButton.image.color = Color.gray;
                HardButton.image.color = Color.green;
                break;
            default:
                break;
        }
    }
}