
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class BrickScript : MonoBehaviour
{
    public int points;
    public int durability;

    //klicano, ko zadanemo brick, zmanjsaj durability, prefarbaj in vrni nov durability
    public int Break()
    {
        durability -= 1;
        this.ChangeColor();
        return durability;
    }

    //spremeni barvo glede na durability, po lastni zelji dodaj vec livelov trdnosti ali spremeni barve
    public void ChangeColor()
    {
        switch (durability)
        {
            case 3:
                this.GetComponent<SpriteRenderer>().color = Color.red;
                break;
            case 2:
                this.GetComponent<SpriteRenderer>().color = Color.yellow;
                break;
            case 1:
                this.GetComponent<SpriteRenderer>().color = Color.green;
                break;
            default: break;
        }
    }
}