using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class GameManager : MonoBehaviour
{
    //rekel bi da so imena tukaj pretty much self explainatory
    public int lives = 3;
    public int score;
    public TextMeshProUGUI livesText;   // Use TextMeshProUGUI instead of Text
    public TextMeshProUGUI scoreText;
    public bool gameOver;
    public GameObject gameoverpanel;
    public GameObject victorypanel;
    public Audiomanagerscript audiomanagerscript;
    void Start()
    {
        // nastavi lives na podlagi izbranega difficultija
        switch (Levels.currentDifficulty)
        {
            case Levels.Difficulty.Easy:
                lives = 5;
                break;
            case Levels.Difficulty.Medium:
                lives = 3;
                break;
            case Levels.Difficulty.Hard:
                lives = 1;
                break;
        }

        UpdateLives(0); // inicializira lives display
        scoreText.text = "Score: " + score;
    }

    //eyyyy good idea Petra. Glede na to, da ta funkcija sprejme parameter, namesto da enostavno zbije eno zivljenje
    //jo lahko Bor reciklira za power up, ki ti doda eno zivljenje ali celo instant death trap, ki ti da gameOver
    //ne glede na stevilo zivljenj
    public void UpdateLives(int changeInLives)
    {
        lives += changeInLives;

        if (lives <= 0)
        {
            lives = 0;  //zivljenje ne more it v -1
            GameOver();
        }

        livesText.text = "Lives: " + lives; //update lives
    }

    public void UpdateScore(int points)
    {
        score += points;
        scoreText.text = "Score: " + score; //update score
    }

    public void GameOver()
    {
        audiomanagerscript.playSound("lost");
        gameOver = true;    //stop
        gameoverpanel.SetActive(true); // se prikaze panel ko izgubis
    }

    public void Win()
    {
        gameOver = true;
        victorypanel.SetActive(true);   //// se prikaze panel ko zmagas
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene("Scenes/SampleScene");
    }

    //quit te trenutno vrne na main menu
    public void Quit()
    {
        SceneManager.LoadScene("Scenes/Menu");
    }
}