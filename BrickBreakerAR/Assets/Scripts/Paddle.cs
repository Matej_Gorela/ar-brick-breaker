using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{

    public GameManager gm;          //gamemanager
    public Ball ball;               //ball
    public GameObject LeftEdge;     //levi rob igre
    public GameObject RightEdge;    //desni rob igre
    float horizontal = 0;           //ni uporabljeno, pustil notri, ce bi zelel alternativen nacin premikanja paddle-a
    public GameObject target;       //nas image target, na katerega bo vezana x pozicija nasega paddle-a
    private Vector3 originalScale;  // originalni scale

    Vector3 paddleposition;         //self explainatory name
    public Audiomanagerscript audiomanagerscript;
    int paddleSpeed = 20;   //v bistvu diktira kolko je pozicija image targeta pomnozena preden jo dodelimo paddlu,
                            //uporabljeno za reverse power up, ki ga samo pomnozi z -1
    public GameObject BrickParent;

    // Start is called before the first frame update
    void Start()
    {
        originalScale = transform.localScale; // shrani originalni scale
    }

    // Update is called once per frame
    void Update()
    {
        if (gm.gameOver)
        {
            return; // stop plosca ko izgubis
        }


        //stara koda od Petre, tukaj samo za referenco, kako je to delovalo
        //ko bo projekt koncan lahko zbrises

        //float horizontal = Input.GetAxis("Horizontal"); //dobimo vrednost med -1 in 1
        //ce pritisnemo levo <- ali a iima vrednost -1
        // ce ne drzimo nobene tipke vr= 0
        // -> ali d vrednost 1 
        //transform.Translate(Vector3.right * horizontal * Time.deltaTime * speed);


        paddleposition = transform.position;            //pridobi pozicijo paddle-a
        paddleposition.x = target.transform.position.x * paddleSpeed;//x vezi na target

        //omeji premikanje paddle-a da ne mora izven igre (y in z sta omejena v scene-u s position constraint komponento
        //ta koscek kode se da improvat, ker je trenutno hardcoded +-1, lahko poskusis uporabit sirino zida in paddle-a)
        if (paddleposition.x < LeftEdge.transform.position.x + ((float)1)) { paddleposition.x = LeftEdge.transform.position.x + ((float)1); }
        if (paddleposition.x > RightEdge.transform.position.x - ((float)1)) { paddleposition.x = RightEdge.transform.position.x - ((float)1); }
        transform.position = paddleposition; //novo pozicijo dodeli nasemu paddle-u

    }

    // Ce powerup zadane paddle 
    void OnTriggerEnter(Collider other)
    {
        // Ce paddle zadane ExtraLife powerup, bo dodalo eno zivljenje in potem takoj unicilo emoji (Heart Eyes emoji)
        if (other.CompareTag("ExtraLife"))
        {
            audiomanagerscript.playSound("oneup");
            Debug.Log("Hit " + other.name);
            gm.UpdateLives(1);
            Destroy(other.gameObject);
        }

        // Ce paddle zadane MinusLife powerup, bo odstranilo eno zivljenje in potem takoj unicilo emoji (Heart Eyes emoji)
        if (other.CompareTag("MinusLife"))
        {
            audiomanagerscript.playSound("onedown");
            Debug.Log("Hit " + other.name);
            gm.UpdateLives(-1);
            Destroy(other.gameObject);
        }

        // Ce paddle zadane InstantDeath powerup, bo instantly končalo igro
        if (other.CompareTag("InstantDeath"))
        {
            Debug.Log("Hit " + other.name);
            gm.UpdateLives(-gm.lives);
            Destroy(other.gameObject);
            ball.rb.velocity = Vector3.zero;
            ball.balspeed = Vector3.zero;
            gm.GameOver();
        }

        // Ce paddle zadane MinusLife powerup, bo odstranilo eno zivljenje in potem takoj unicilo emoji (Heart Eyes emoji)
        if (other.CompareTag("PaddleSize"))
        {
            audiomanagerscript.playSound("resize");
            Debug.Log("Hit " + other.name);
            ChangePaddleSize();
            Destroy(other.gameObject);
        }
        //fu** you and your stupid muscle memory
        if (other.CompareTag("reverse"))
        {
            audiomanagerscript.playSound("reverse");
            Debug.Log("Hit " + other.name);
            paddleSpeed = paddleSpeed * (-1);
            Destroy(other.gameObject);
        }
        //We're bringing it, we're bringing it, We're bringing it back! FIREBALL!
        if (other.CompareTag("fireball"))
        {
            audiomanagerscript.playSound("fireball");
            Debug.Log("Hit " + other.name);
            for (int i = 0; i < BrickParent.transform.childCount; i++)
            {
                BrickParent.transform.GetChild(i).GetComponent<BoxCollider>().isTrigger = true;
                ball.GetComponent<ParticleSystem>().Play();
            }
            Destroy(other.gameObject);
        }
    }

    private void ChangePaddleSize()
    {
        // randomly zmanjsa ali poveca paddle
        float sizeChange = UnityEngine.Random.Range(0.5f, 2.0f);

        // adjusta scale
        transform.localScale = new Vector3(originalScale.x * sizeChange, originalScale.y, originalScale.z);
    }

    //stare funkcije, ki niso v uporabi, lahko bi bile uporabljene za alterativno metodo premikanja
    //(npr. en image target za levo in en za desno, lahko bi bli nalepljeni na cilindru kot kaksen pringles can ali
    //na kaksni kocki, da dodamo nekaksen cuden home made peripheral)
    public void MoveLeft()
    {
        horizontal = -1;
    }

    public void MoveRight()
    {
        horizontal = 1;
    }

    public void StopMoving()
    {
        horizontal = 0;
    }
}