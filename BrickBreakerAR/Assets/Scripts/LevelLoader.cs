using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LevelLoader : MonoBehaviour
{

    public GameObject BrickPrefab;  //Referenca na brick prefab
    public GameObject BrickParent;  //Objekt, na katerega bomo vezali nase bricke
    GameObject BrickSpawn;          //Novi bricki bodo dosegljivi pod tem imenom
    public bool GameLoaded = false; //Boolean uporabljen, da Ball.cs pocaka preden se zacne igra (glej Ball.cs)
    public int currentLevel = 1;    //skriptu pove, kateri livel naj load-a
    public int levelCount = 5;      //stevilo livelov v nasi igri POZOR!!! Ne pozabi povecat ko dodas livele
                                    //(preveri tudi v scen-u, da se ne bi vrednosti povozile med sabo (glej LevelLoaderObject)).

    // Start is called before the first frame update
    void Start()
    {
        loadLevel(currentLevel);

    }

    // Update is called once per frame
    void Update()
    {

    }

    //Tukaj preberemo txt file in vrnemo string, ki ga bomo uporabili za load-at livel
    public static string ReadString(int level)
    {
        TextAsset mytxt = Resources.Load<TextAsset>("Level" + level);
 
        return mytxt.ToString();
    }

    //metoda ki nalozi livel
    public void loadLevel(int level)
    {
        float brickX = -7.5f;                       //tukaj se zacnejo x pozicije brickov (najbolj levi bricki)
        float brickY = 0f;                          //tu se zacnejo y pozicije brickov (najbolj zgornji bricki)
        string levelDesign = ReadString(level);     //pridobi string za livel
        char[] chars = levelDesign.ToCharArray();   //razsekaj string v array znakov
        for (int i = 0; i < chars.Length; i++)
        {
            //tu za vsak char iz nasega arraya izvedemo akcijo, ki temu char-u pripada
            char c = chars[i];
            switch (c)
            {
                case '1':
                    BrickSpawn = Instantiate(BrickPrefab);                                  //ustvari nov brick
                    BrickSpawn.transform.SetParent(BrickParent.transform);                  //dodaj ga BrickParentu
                    BrickSpawn.transform.localPosition = new Vector3(brickX, brickY, 0f);   //nastimaj mu pozicijo glede na starsa
                    BrickSpawn.transform.localScale = Vector3.one;                          //poskrbi da je njegova velikost 1
                    BrickSpawn.GetComponent<BrickScript>().durability = 1;                  //trdnost 1 -> unicen z enim udarcem
                    BrickSpawn.GetComponent<BrickScript>().ChangeColor();                   //prefarbaj brick v pravo barvo
                                                                                            //ce ne dobi default barvo prefaba
                    break;
                case '2':
                    BrickSpawn = Instantiate(BrickPrefab);
                    BrickSpawn.transform.SetParent(BrickParent.transform);
                    BrickSpawn.transform.localPosition = new Vector3(brickX, brickY, 0f);
                    BrickSpawn.transform.localScale = Vector3.one;
                    BrickSpawn.GetComponent<BrickScript>().durability = 2;
                    BrickSpawn.GetComponent<BrickScript>().ChangeColor();
                    break;
                case '3':
                    BrickSpawn = Instantiate(BrickPrefab);
                    BrickSpawn.transform.SetParent(BrickParent.transform);
                    BrickSpawn.transform.localPosition = new Vector3(brickX, brickY, 0f);
                    BrickSpawn.transform.localScale = Vector3.one;
                    BrickSpawn.GetComponent<BrickScript>().durability = 3;
                    BrickSpawn.GetComponent<BrickScript>().ChangeColor();
                    break;
                case ',':
                    brickX += 1.5f; //v primeru da je znak "," se premaknemo za en brick v desno
                    break;
                case '\n':
                    brickX = -7.5f; //ce je new line, se vrenmo na startni x in gremo v naslednjo vrstico
                    brickY -= 0.5f;
                    break;
            }
        }
        GameLoaded = true;          //ko je livel koncal load-at to sporoci preko GameLoaded bool-a (glej Ball.cs)
    }

    //koda za randomly generated livel, trenutno ni uporabljena, a je tukaj, ce bi zeleli infinite mode
    public void RandomLevel()
    {
        float brickX = -7.5f;
        float brickY = 0f;
        //visina gre lahko tudi dlje kot 5, priporocam da ne cez 10,
        //possible upgrade te funkcije je, da sirino in visino dolocimo s parametri, namesto da je hardcoded
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 11; j++)//trenutno narejeno na 11 brickov v sirino, pazi, ce spreminjas, da ne zjebes poravnave
            {
                //koda zelo podobna kot pri load-anju livela iz txt file-a, razlika je ve temu, da je durability
                //dolocen z random
                int r = Random.Range(0, 4);
                if (r == 0) { }
                else
                {
                    BrickSpawn = Instantiate(BrickPrefab);
                    BrickSpawn.transform.SetParent(BrickParent.transform);
                    BrickSpawn.transform.localPosition = new Vector3(brickX, brickY, 0f);
                    BrickSpawn.transform.localScale = Vector3.one;
                    BrickSpawn.GetComponent<BrickScript>().durability = r;
                    BrickSpawn.GetComponent<BrickScript>().ChangeColor();
                }
                brickX += 1.5f;
            }
            brickY -= 0.5f;
            brickX = -7.5f;
        }
        GameLoaded = true;
    }
}