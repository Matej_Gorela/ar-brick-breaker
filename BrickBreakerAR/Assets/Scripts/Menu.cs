using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public GameObject settingsPanel; // Dodamo referenco na panel

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Quit"); //ker ne bo dejansko zaprlo igre damo log
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Scenes/SampleScene");
    }

    public void Settings()
    {
        settingsPanel.SetActive(true); // Preklopi vidnost panela
        //Debug.Log("Panel "); // Log za preverjanje
    }
}