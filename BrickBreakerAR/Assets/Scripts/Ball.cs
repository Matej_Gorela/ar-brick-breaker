using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;

public class Ball : MonoBehaviour
{

    public Rigidbody rb;            //rigidbody nase zoge
    public bool play;               //zoga je v igri
    public Transform paddle;        //referenca na paddle
    public float speed;             //mogoce bo treba spremenit za mobilne zaslone, trenutna vrednost testirana le na laptopu
    public GameManager gm;          //game manager
    public Vector3 balspeed;        //hitrost zoge
    public GameObject BrickGroup;   //v bistvu isti objekt kot BrickParent v LevelLoader.cs -> parent objekt, ki drzi bricke

    public LevelLoader levelLoader; //levelloader
    public Transform powerup1; // za powerups
    public Transform powerup2; // za powerups
    public Transform powerup3; // za powerups
    public Transform powerup4; // za powerups
    public Transform powerup5;
    public Transform powerup6;
    public Audiomanagerscript audiomanagerscript;
    public ParticleSystem brickParticles;

    // Start is called before the first frame update

    IEnumerator Start()
    {    //namesto void je IEnumerator, ker s tem lahko naredim ta cuden while yield crap
         //ki ga vidite tu spodaj, da pocaka da livel nafila, ce bi uporabu samo while se zacikla
         //nisem cisto preprican za kaj se gre ampak je some parallel programing bullsh**
        while (!levelLoader.GameLoaded) { yield return null; }

        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    public void Update()
    {
        //ce je stevilo brickov enako 0 in igra se ni koncana smo zmagali
        if ((BrickGroup.transform.childCount == 0) && (!gm.gameOver))
        {
            if (levelLoader.currentLevel >= levelLoader.levelCount)
            {
                gm.Win();
            }
            else
            {
                levelLoader.currentLevel++;
                levelLoader.loadLevel(levelLoader.currentLevel);

            }
            play = false;               //zoga ni vec v igri

            rb.velocity = Vector3.zero; //hitrost daj na 0
        }

        //ce zoga ni v igri
        if (!play)
        {
            GetComponent<ParticleSystem>().Stop();      //ce je fireball effect aktiven ga to izklopi
            for (int i = 0; i < BrickGroup.transform.childCount; i++)
            {
                //ce so bli bricki dani na trigger (fireball power up) jih vrni na collision
                BrickGroup.transform.GetChild(i).GetComponent<BoxCollider>().isTrigger = false;
            }
            transform.position = paddle.position;   //ce zoga ni v igri jo resetiraj na paddle
        }

        if (gm.gameOver)
        {
            rb.velocity = Vector3.zero;             //ustavi zogo
            transform.position = paddle.position;   //resetiraj zogo na paddle
            return; //stop
        }

        //V bistvu je ta koda sedaj nepotrebna, ker je ta kos kode vezan na drugi image target, pride prav med
        //testiranjem na pc-ju, da lahko samo kliknes space namesto da izvadis image target -> rajsi ne zbrisi
        //pristisni space da se zacne igra
        if (Input.GetButtonDown("Jump") && !play)
        { //ne spremeni hitrosti v igri
            play = true;
            rb.AddForce(0, (float)0.0001, 0);
        }

        //pridobi hitrost zoge
        balspeed = rb.velocity;
        //ce absolutna vrednost hitrosti ni 25 gre prepocasi ali prehitro, dejansko za to kodo ni vazno,
        //kateri od teh scenarijev je, vazno je, da hitrost damo nazaj na pravo vrednost, odvisno od tega ali gre gor ali dol 
        //Na podoben nacin omeji horizontalno hitrost, horizontalna hitrost naj bo omejena na isto vrednost kot 
        //vertikalna, tako bo zoga potovala pod kotom najvec 45 stopinj, trenutno hardcodano, 25 lahko zamenjas s kaksno
        //spremenljivko za vecjo fleksibilnost kode, kar bi mogoce lahko prislo prav, ce bo treba spreminjat za mobilne zaslone
        if (math.abs(balspeed.y) != 25)
        {
            if (balspeed.y < 0)
            {
                balspeed.y = -25;
            }
            else { balspeed.y = 25; }
        }
        if (balspeed.x > 25) { balspeed.x = 25; }
        if (balspeed.x < -25) { balspeed.x = -25; }
        rb.velocity = balspeed;
    }

    //funkcija ki zazna da je zoga padla izven polja
    //hihi jaz pa sem jo recikliral se za moje fireball zadeve XD
    void OnTriggerEnter(Collider other)
    {
        balspeed = rb.velocity;
        if (other.CompareTag("bottom"))
        {
            audiomanagerscript.playSound("ouch");
            // Debug.Log("Game Over"); //nisi ujel zoge
            balspeed = Vector3.zero; //vektor na poziciji 0.0
            play = false;
            gm.UpdateLives(-1);//izgubimo eno zivljenje
            rb.velocity = balspeed;
        }
        else if (other.CompareTag("birck")) //ta del kode se zazene v primeru fireball power upa ce gremo cez brick
        {
            brickParticles.transform.position = other.transform.position;//doloci pozicijo particle effecta na pozicijo bricka
            brickParticles.startColor = other.transform.GetComponent<SpriteRenderer>().color;//barva particla = barva bricka
            brickParticles.Emit(10);        //emitaj 10 particlov
            gm.UpdateScore(other.gameObject.GetComponent<BrickScript>().points);
            other.GetComponent<BrickScript>().Break();  //zlomi brick
            if (other.gameObject.GetComponent<BrickScript>().durability <= 0)
            {
                Destroy(other.gameObject); //razbi
            }
            spawnPowers(other.transform);   //spawnaj powerup
        }
    }

    void OnCollisionEnter(Collision other)
    {
        //to je dano v if ker je ce ne ball v bistvu konstantno imel kolizijo s paddlom ker je tisti paddle position
        //v colliderju od paddla tako da si konstantno cul ta jeb*** plink plink PLINK PLIIIIIIINNNNKKKK!!!!
        //Zato je zdaj v if-u, da dela plink samo ko se zoga premika
        if (play) { audiomanagerscript.playSound("plink"); }

        //other je referenca na objekt s katerim se je zgodila kolizija
        if (other.transform.CompareTag("birck"))
        { //ce smo se zabili v brick
          //(hahaha "birck", prehitre prste mas Petra)

            brickParticles.transform.position = other.transform.position;//kje spawna particle effects
            brickParticles.startColor = other.transform.GetComponent<SpriteRenderer>().color;//kake barve naj so (iste ko brick)
            brickParticles.Emit(10);//spawnaj jih 10

            spawnPowers(other.transform);//spawnaj power up
            //pojdi na gameobject in pridobi od njega brick..
            //pojdi v scripto in pridobi vrednost
            gm.UpdateScore(other.gameObject.GetComponent<BrickScript>().points);
            other.gameObject.GetComponent<BrickScript>().Break();

            if (other.gameObject.GetComponent<BrickScript>().durability <= 0)
            {
                Destroy(other.gameObject); //razbi
            }
        }
    }
    //nimam pojma zakaj sem dal ime jumper, ta koda starta zogo, zazene jo drugi image target
    public void jumper()
    {
        if (!play)
        { //ne spremeni hitrosti v igri
            play = true;
            rb.AddForce(0, (float)0.0001, 0);
        }
    }

    //Spostovani Bor. Opazil sem, da Vasa koda za spawnanje powerupa potrebuje le transform objekta in ne objekt sam.
    //Odlocil sem se torej, da jo prenesem v to funkcijo in ji kot parameter dam le transform objekta. S tem sem dosegel, da
    //lahko isti kos kode uporabim za normalno zogo, ko je zazeleni objekt kolizija ter za moj FIREBALLLL MOTHERFU**** power up,
    //ko je zazeleni objekt collider. Lep pozdrav. Matej Gorela
    // Dobro, LP.
    void spawnPowers(Transform otherTransform)
    {
        int randChance2 = (UnityEngine.Random.Range(0, 1000) % 3);

        if (randChance2 == 0) 
        {
            int randChance = (UnityEngine.Random.Range(1, 1001) % 6);
            if (randChance == 0)
            {
                Instantiate(powerup1, otherTransform.position, otherTransform.rotation);
            }
            else if (randChance == 1)
            {
                Instantiate(powerup2, otherTransform.position, otherTransform.rotation);
            }
            else if (randChance == 2)
            {
                Instantiate(powerup3, otherTransform.position, otherTransform.rotation);
            }
            else if (randChance == 3)
            {
                Instantiate(powerup4, otherTransform.position, otherTransform.rotation);
            }
            else if (randChance == 4) 
            { 
                Instantiate(powerup5, otherTransform.position, otherTransform.rotation); 
            }
            else if (randChance == 5) 
            { 
                Instantiate(powerup6, otherTransform.position, otherTransform.rotation); 
            }
        }
    }
}