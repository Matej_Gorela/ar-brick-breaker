using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audiomanagerscript : MonoBehaviour
{

    public AudioSource audioSource;
    public AudioClip plink;
    public AudioClip ouch;
    public AudioClip oneup;
    public AudioClip onedown;
    public AudioClip lost;
    public AudioClip resize;
    public AudioClip reverse;
    public AudioClip fireball;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void playSound(string clipName)
    {
        switch (clipName)
        {
            case "ouch":
                audioSource.PlayOneShot(ouch);
                break;
            case "plink":
                audioSource.PlayOneShot(plink);
                break;
            case "oneup":
                audioSource.PlayOneShot(oneup);
                break;
            case "onedown":
                audioSource.PlayOneShot(onedown);
                break;
            case "lost":
                audioSource.PlayOneShot(lost);
                break;
            case "resize":
                audioSource.PlayOneShot(resize);
                break;
            case "reverse":
                audioSource.PlayOneShot(reverse);
                break;
            case "fireball":
                audioSource.PlayOneShot(fireball);
                break;
            default: break;
        }
    }
}